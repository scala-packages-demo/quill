object QuillDemo extends App {
println("Hello, World!")
// import $ivy.{
    // `io.getquill::quill-jdbc:3.5.2`, // 3.4.10
    // `org.postgresql:postgresql:42.2.10`, // 42.2.8
    // `com.opentable.components:otj-pg-embedded:0.13.1`,
    // `ch.qos.logback:logback-classic:1.2.3`
// }
// import com.opentable.db.postgres.embedded.EmbeddedPostgres
// val server = EmbeddedPostgres.builder().setPort(5432).start()


// import io.getquill._
// import io.getquill.{PostgresJdbcContext, LowerCase}
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
// val pgDataSource = new org.postgresql.ds.PGSimpleDataSource()
val pgDataSource = new org.postgresql.ds.PGSimpleDataSource()
// pgDataSource.setUser("postgres")
val config = new HikariConfig()
config.setDataSource(pgDataSource)
// val ctx = new PostgresJdbcContext(LowerCase, new HikariDataSource(config))
// import ctx._

case class City(id: Int, 
                name: String, 
                countryCode: String, 
                district: String, 
                population: Int
)

// ctx.run(query[City])

}

