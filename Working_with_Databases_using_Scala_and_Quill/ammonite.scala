println("Hello, World!")
import $ivy.{
    `io.getquill::quill-jdbc:3.5.2`, // 3.4.10
    `org.postgresql:postgresql:42.2.10`, // 42.2.8
    `ch.qos.logback:logback-classic:1.2.3`
}

import io.getquill.{PostgresJdbcContext, LowerCase}
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
val pgDataSource = new org.postgresql.ds.PGSimpleDataSource()
val config = new HikariConfig()
config.setDataSource(pgDataSource)
val ctx = new PostgresJdbcContext(LowerCase, new HikariDataSource(config))
import ctx._

case class City(
    id: Int, 
    name: String, 
    countryCode: String, 
    district: String, 
    population: Int
)

case class Country(
    code: String, 
    name: String, 
    continent: String, 
    region: String,
    population: Int
)
    // surfaceArea: Double,
    // indepYear: Option[Int],
    // lifeExpectancy: Option[Double],
    // gnp: Option[scala.math.BigDecimal],
    // gnpold: Option[scala.math.BigDecimal],
    // localName: String,
    // governmentForm: String,
    // headOfState: Option[String],
    // capital: Option[Int],
    // code2: String

case class CountryLanguage(
    countrycode: String, 
    language: String,
    isOfficial: Boolean,
    percentage: Double
)

ctx.run(query[City])
ctx.run(query[Country])
ctx.run(query[CountryLanguage])

ctx.run(query[City].filter(_.name == "Singapore"))
ctx.run(query[City].filter(_.id == 3208))
ctx.run(query[City].filter(_.population > 9000000))
ctx.run(query[City].filter(c => c.population > 5000000 && c.countryCode == "CHN"))
ctx.run(query[City].filter(_.population > 5000000).filter(_.countryCode == "CHN"))

def find(cityId: Int) = ctx.run(query[City].filter(_.id == lift(cityId)))
find(3208)
find(3209)

// Errors: do not compile
ctx.run(query[City].filter(_.name.length == 1))
ctx.run(query[City].filter(_.name.substring(0, 1) == "S"))

ctx.run(query[Country].map(c => (c.name, c.continent)))
ctx.run(query[Country].map(c => (c.name, c.continent, c.population)))

def findName(cityId: Int) = ctx.run(query[City].filter(_.id == lift(cityId)).map(_.name))
findName(3208)
findName(3209)

ctx.run(
    query[City]
        .join(query[Country])
        .on{case (city, country) => city.countryCode == country.code}
        .filter{case (city, country) => country.continent == "Asia"}
        .map{case (city, country) => city.name}
)

ctx.run(query[City].insert(City(10000, "test", "TST", "Test County", 0)))
ctx.run(query[City].filter(_.population == 0))

ctx.run(
    liftQuery(List(
      City(10001, "testville", "TSV", "Test County", 0)  ,
      City(10002, "testopolis", "TSO", "Test County", 0),
      City(10003, "testberg", "TSB", "Test County", 0)
    )).foreach(e => query[City].insert(e))
)
ctx.run(query[City].filter(_.population == 0))

ctx.run(query[City].filter(_.id == 10000).update(City(10000, "testham", "TST", "Test County", 0)))
ctx.run(query[City].filter(_.id == 10000))

ctx.run(query[City].filter(_.id == 10000).update(_.name -> "testford"))
ctx.run(query[City].filter(_.id == 10000))

ctx.run(query[City].filter(_.district == "Test County").update(_.district -> "Test Borough"))
ctx.run(query[City].filter(_.population == 0))

// Error
ctx.transaction{
    ctx.run(query[City].filter(_.district == "Test Borough").update(_.district -> "Test County"))
    throw new Exception()
}
ctx.run(query[City].filter(_.population == 0))

// Inspirated by
// Working with Databases using Scala and Quill
// https://www.lihaoyi.com/post/WorkingwithDatabasesusingScalaandQuill.html