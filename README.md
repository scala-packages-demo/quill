# quill

Compile-time Language Integrated Queries for Scala. [quill](https://index.scala-lang.org/getquill/quill)

[![quill Scala version support](https://index.scala-lang.org/zio/zio-quill/quill/latest-by-scala-version.svg?platform=jvm)](https://index.scala-lang.org/zio/zio-quill/quill)

* [*Get programming with Scala*
  ](https://www.worldcat.org/search?q=ti%3AGet+Programming+with+Scala)
  2021-09 Daniela Sfregola
  * Ch. 46 Database queries with Quill

* [*Working with Databases using Scala and Quill*
  ](https://www.lihaoyi.com/post/WorkingwithDatabasesusingScalaandQuill.html)
  2019-10 Li Haoyi

# API
## PostgresJdbcContext
### 2nd argument type possibilities
<dl>
  <dt>configPrefix: String</dt>
  <dd>Name of configurtion file?</dd>

  <dt>config: com.typesafe.config.Config</dt>
  <dd>
  </dd>

  <dt>config: io.getquill.JdbcContextConfig</dt>
  <dd>
  </dd>

  <dt>dataSource: javax.sql.DataSource with java.io.Closeable</dt>
  <dd>
  </dd>
</dl>
